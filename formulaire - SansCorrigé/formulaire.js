
// Exo: verifiez si les mots de passes saisis sont :
// - identiques
// - longueur min de 6
// - contienent au moins un chiffre

var form = document.getElementById("monFormulaire");

form.addEventListener("submit", function (e){ 
		//pour récupérer la valuer d'un composant input "Eltinput.value;""

		var mdp1 = document.getElementById("mdp1").value;
		var mdp2 = document.getElementById("mdp2").value;
		var motif = /[0-9]/;
				if (motif.test(mdp1)){
					if(mdp1.length >= 6){
						if(mdp1 === mdp2){
							if(document.getElementById("conditions").checked){
								document.getElementById("message").innerHTML = '<span style="color: green;" id="message">Logged in succesfully !</span>';
							}else{
								document.getElementById("message").innerHTML = '<span style="color: red;" id="message">You must agree to the terms and conditions !</span>';
							}
						}else{
							document.getElementById("message").innerHTML = '<span style="color: red;" id="message">passwords are not similar !</span>';
						}
					}else{
						document.getElementById("message").innerHTML = '<span style="color: red;" id="message">password must contain at least 6 charachters !</span>';
					}
				}else{
					document.getElementById("message").innerHTML = '<span style="color: red;" id="message">password must contain at least one number !</span>';		
				}

		e.preventDefault(); //Stop la soumission cad le fonctionnement par default de submit
});

